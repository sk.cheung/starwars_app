# Star Wars Movies App
This is a basic movie app for Star Wars fans, which is based on a YouTube video tutorial made by [Ryan Hemrick](https://www.youtube.com/watch?v=TVrfhONc8Jw), but the routes are refactored by using Express Router.

## Pre-requisite
In order to run this app for local development, you need to have **NodeJS** installed.

Then, clone this repository to your local development environment and run `npm install` on terminal to install all dependencies.

To run the server, simply run the command `node app.js` on terminal, and browse the app on `localhost:3000`.

                                                                                                  