var express    = require('express'),
    app        = express(),
    path = require('path'),
    indexRoute = require('./routes/index');

app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', indexRoute);

app.listen(process.env.PORT || 3000, function () {
    console.log('The application is running');
});
