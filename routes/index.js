var express = require('express'),
    router  = express.Router(),
    moviesJSON = require('../movies.json');

// Routes

// home
router.get('/', function (req, res) {
    var movies = moviesJSON.movies;
    res.render('home', {
        title: 'Star Wars Movies',
        movies: movies
    });
});

// movie_single
router.get('/star_wars_episode/:episode_number?', function (req, res) {
    var episode_number = req.params.episode_number;
    var movies = moviesJSON.movies;
    if (episode_number >= 1 && episode_number <= 6) {
        var movie = movies[episode_number - 1];
        var mainCharacters = movie.main_characters;
        res.render('movie_single', {
            title: movie.title,
            movies: movies,
            movie: movie,
            mainCharacters: mainCharacters
        });
    } else {
        res.render('notFound', {
            title: 'This is not the page you\'re looking for',
            movies: movies
        });
    }
});

// notFound
router.get('*', function (req, res) {
    var movies = moviesJSON.movies;
    res.render('notFound', {
        movies: movies,
        title: 'This is not the page you\'re looking for'
    });
});

module.exports = router;